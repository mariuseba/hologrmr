using System;
using UnityEngine;
using System.Collections.Generic;

namespace CustomAssets
{
	public interface IRecordableObject
	{
		
		GameObject BaseObjectReference
		{
			get; set;
		}
		
		Transform LastTransform
		{
			get; set;
		}
		
		IDictionary<float, Transform> Movement
		{
			get; set;
		}
	}
}

