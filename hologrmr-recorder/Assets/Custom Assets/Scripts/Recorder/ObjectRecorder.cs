﻿using UnityEngine;
using System.Collections;
using CustomAssets;

public class ObjectRecorder : MonoBehaviour {
	
	#region Private Properties
	private float _timeRecordingStarted;
	private bool _recordingStarted;
	#endregion
	
	#region Public Properties
	public IRecordableObject[] RecordedObjects;
	#endregion
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		_recordNewPosition();
	}
	
	private void _recordNewPosition()
	{
		if(_recordingStarted == true && _timeRecordingStarted == null)
		{
			_timeRecordingStarted = Time.time;
		}
		
		if(RecordedObjects != null)
			for(int i = RecordedObjects.Length; i >= 0; i--)
			{
				if(RecordedObjects[i].BaseObjectReference.transform != RecordedObjects[i].LastTransform)
				{
					RecordedObjects[i].Movement.Add(Time.time - _timeRecordingStarted, RecordedObjects[i].BaseObjectReference.transform);
					Debug.Log ("new stuff >\r\n" + (Time.time - _timeRecordingStarted).ToString() + " : " + RecordedObjects[i].BaseObjectReference.transform.ToString());
				}
			}		
	}
}
